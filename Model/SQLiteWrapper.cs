﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using InventorySetup.Controller;
using NLog;

namespace InventorySetup.Model
{
    class SQLiteWrapper
    {
        private string ConnStr;
        private SQLiteCommand Cmd;
        private static Logger log = LogManager.GetLogger("SQLiteWrapper");
        private int RowsUpdated = -1;

        public SQLiteWrapper(string dbFile)
        {
            string file = "";

            if (File.Exists(dbFile))
                file = dbFile;
            else
                file = "inventory.db";

            ConnStr = string.Format("Data Source={0}", file);
        }

        public SQLiteWrapper(Dictionary<string, string> connectionOptions)
        {
            StringBuilder tmp = new StringBuilder();

            foreach (KeyValuePair<string, string> kvp in connectionOptions)
            {
                tmp.AppendFormat("{0}={1}; ", kvp.Key, kvp.Value);
            }

            ConnStr = tmp.ToString().Trim("; ".ToCharArray());
        }

        private SQLiteConnection Connect()
        {
            SQLiteConnection link = null;

            try
            {
                link = new SQLiteConnection(ConnStr);
                link.Open();
            }
            catch (SQLiteException e)
            {
                log.Error("Could not connect to SQLite: {0}", e);
            }

            return link;
        }

        private void Close()
        {
            if (Cmd != null)
            {
                if (Cmd.Connection != null)
                {
                    try
                    {
                        Cmd.Connection.Close();
                        Cmd.Parameters.Clear();
                        Cmd.Dispose();
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("Error trying to close Database Connection: {0}", e);
                    }
                }
            }
        }

        #region Insert
        public int Insert(string table, List<List<object>> values)
        {
            return Insert(table, "", values, "");
        }

        public int Insert(string table, List<string> columns, List<List<object>> values)
        {
            return Insert(table,
                String.Join(",", columns.ToArray()),
                values,
                "");
        }

        public int Insert(string table, List<string> columns, List<List<object>> values, string extra)
        {
            return Insert(table, String.Join(",", columns.ToArray()), values, extra);
        }

        public int Insert(string table, string columns, List<List<object>> values, string extra)
        {
            RowsUpdated = 0;

            if (table.Length > 0)
            {
                if (values.Count > 0)
                {
                    Cmd = new SQLiteCommand();
                    StringBuilder query = new StringBuilder("INSERT INTO ");
                    query.Append(table);

                    if (columns.Length > 0)
                    {
                        query.AppendFormat(" ({0})", columns);
                    }

                    query.Append(" VALUES ");
                    int index = 0;
                    StringBuilder rowValues = null;

                    foreach (List<object> row in values)
                    {
                        query.Append("(");
                        rowValues = new StringBuilder();

                        foreach (object value in row)
                        {
                            string pname = string.Format("@Param{0}", index);
                            rowValues.AppendFormat("{0},", pname);
                            Cmd.Parameters.Add(new SQLiteParameter(pname, value));
                            index++;
                        }

                        query.AppendFormat("{0}),", rowValues.ToString().Trim(','));
                        rowValues = null;
                    }

                    query = new StringBuilder(query.ToString().Trim(','));

                    if (extra.Length > 0)
                        query.Append(extra);

                    Cmd.Connection = Connect();
                    Cmd.CommandText = query.ToString();
                    log.Debug("Insert query: {0}", Cmd.CommandText);

                    try
                    {
                        RowsUpdated = Cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("An error ocurred while trying to insert data: {0}", e);
                        RowsUpdated = -1;
                    }

                    Close();
                }
            }
            else
            {
                log.Error("Missing table name!");
            }

            return RowsUpdated;
        } // End Insert

        #endregion Insert

        public int Update(string table, Dictionary<string, object> data, SQLWhere where)
        {
            RowsUpdated = 0;

            if (table.Length > 0)
            {
                if (data.Count > 0)
                {
                    Cmd = new SQLiteCommand();
                    StringBuilder query = new StringBuilder("UPDATE ");
                    query.Append(table);
                    string sqlExp = "";
                    SQLiteParameter[] paramArray = null;

                    if (where != null)
                    {
                        sqlExp = where.GetQueryString();

                        if (sqlExp.Length > 0)
                        {
                            paramArray = where.GetParamsAsArray();
                            query.AppendFormat(" {0}", sqlExp);

                            if (paramArray.Length > 0)
                            {
                                Cmd.Parameters.AddRange(paramArray);
                            }

                            sqlExp = "";
                            paramArray = null;
                        }
                    }
                    else
                    {
                        log.Warn("Updated without a Where condition!");
                    }

                    Cmd.Connection = Connect();
                    Cmd.CommandText = query.ToString();
                    query = null;
                    log.Debug("Update Query: {0}", Cmd.CommandText);

                    try
                    {
                        RowsUpdated = Cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        log.Error("Update Error: {0}", e.StackTrace);
                    }

                    Close();
                }
                else
                {
                    log.Warn("Update Error: data Dictionary contains {0} elements! Nothing to update.", data.Count);
                }
            }
            else
            {
                log.Error("Update Error: missing table name.");
                RowsUpdated = -1;
            }

            return RowsUpdated;
        }

        #region Query

        public DataTable Query(string table)
        {
            return Query(table, "", "", null, "", "", 0, 0);
        }

        public DataTable Query(string table, List<string> columns)
        {
            return Query(table, "", String.Join(",", columns.ToArray()), null, "", "", 0, 0);
        }

        public DataTable Query(string table, string join, string columns)
        {
            return Query(table, join, columns, null, "", "", 0, 0);
        }

        public DataTable Query(string table, string columns, SQLWhere where)
        {
            return Query(table, "", columns, where, "", "", -1, -1);
        }

        public DataTable Query(string table, string join, string columns, SQLWhere where, string orderBy, string groupBy, int limit, int offset)
        {
            DataTable r = null;

            if (table.Length > 0)
            {
                Cmd = new SQLiteCommand();
                StringBuilder query = new StringBuilder("SELECT ");

                if (columns.Length > 0)
                    query.Append(columns);
                else
                    query.Append("*");

                query.AppendFormat(" FROM {0}", table);

                if (join != null && join.Length > 0)
                {
                    query.AppendFormat(" {0}", join);
                }

                SQLiteParameter[] paramArray = null;
                string queryStr = "";

                if (where != null)
                {
                    queryStr = where.GetQueryString();

                    if (queryStr.Length > 0)
                    {
                        log.Debug("Query.Where = {0}", queryStr);
                        query.AppendFormat(" {0}", queryStr);
                        paramArray = where.GetParamsAsArray();

                        if (paramArray.Length > 0)
                        {
                            log.Debug("Query.Where.Params.Count = {0}", paramArray.Length);
                            Cmd.Parameters.AddRange(paramArray);
                        }
                    }

                    queryStr = "";
                    paramArray = null;
                }

                if (orderBy != null && orderBy.Length > 0)
                {
                    query.AppendFormat(" {0}", orderBy);
                }

                if (groupBy != null && groupBy.Length > 0)
                {
                    query.AppendFormat(" {0}", groupBy);
                }

                if (limit > 0)
                {
                    query.AppendFormat(" LIMIT {0}", limit);
                }

                if (offset > 0)
                {
                    query.AppendFormat(" OFFSET {0}", offset);
                }

                Cmd.Connection = Connect();
                Cmd.CommandText = query.ToString();
                log.Debug("Query String: {0}", Cmd.CommandText);
                query = null;

                try
                {
                    SQLiteDataReader reader = Cmd.ExecuteReader();
                    r = new DataTable();
                    r.Load(reader);
                    reader.Close();
                }
                catch (SQLiteException e)
                {
                    log.Error("Could not execute query: {0}", e.StackTrace);
                }

                Close();
            }
            else
            {
                log.Debug("Missing table name!");
            }

            return r;
        } // End of Query();

        #endregion Query

        #region SQLite Helper Classes
        internal class SQLExpression
        {
            protected int ParamIndex;
            protected StringBuilder Query;
            protected List<SQLiteParameter> ParamList;
            protected string ParamNameFormat = "@Param{0}";

            public SQLExpression()
                :this("")
            {
            }

            protected SQLExpression(string initial)
            {
                Query = new StringBuilder(initial);
                initial = initial.Replace(" ", "").Trim();

                if (initial.Length <= 0)
                    initial = "Param";

                ParamList = new List<SQLiteParameter>();
                ParamNameFormat = string.Format("@{0}{{0}}", initial);
            }

            internal List<SQLiteParameter> GetParams()
            {
                return ParamList;
            }

            internal SQLiteParameter[] GetParamsAsArray()
            {
                return ParamList.ToArray();
            }

            internal virtual string GetQueryString()
            {
                return Query.ToString();
            }
        }

        public class SQLWhere : SQLExpression
        {
            public SQLWhere()
                : base("WHERE ")
            {
            }

            public SQLWhere Append(string expr, object paramValue)
            {
                object[] tmp = new object[] { paramValue };
                return Append(expr, tmp);
            }

            public SQLWhere Append(string condition)
            {
                Query.Append(condition);
                return this;
            }

            public SQLWhere Append(string condition, object[] paramValues)
            {
                if (paramValues != null && paramValues.Length > 0)
                {
                    List<string> paramNames = null;

                    foreach (object param in paramValues)
                    {
                        string paramName = string.Format(ParamNameFormat, ParamIndex);
                        paramNames.Add(paramName);
                        ParamList.Add(new SQLiteParameter(paramName, param));
                        ParamIndex++;
                    }

                    Query.AppendFormat(condition, paramNames.ToArray());
                }
                else
                {
                    Query.Append(condition);
                }

                return this;
            }

        } // End class SQLWhere

        public class SQLGroupBy : SQLExpression
        {
            public SQLGroupBy()
                : base("GROUP BY ")
            {
            }

            protected SQLGroupBy(string init)
                : base(init)
            {
            }

            public SQLGroupBy AddParams(object[] values)
            {
                if (values.Length > 0)
                {
                    foreach (object value in values)
                    {
                        AddParam(value);
                    }
                }

                return this;
            }

            public SQLGroupBy AddParam(object value)
            {
                string paramName = string.Format(ParamNameFormat, ParamIndex);
                ParamList.Add(new SQLiteParameter(paramName, value));
                Query.AppendFormat("{0},", paramName);
                ParamIndex++;
                return this;
            }

            override internal string GetQueryString()
            {
                return Query.ToString().Trim(',');
            }
        } // End class SQLGroupBy

        public class SQLOrderBy : SQLGroupBy
        {
            private OrderType Type;

            public enum OrderType
            {
                ASCENDING, DESCENDING
            }

            public SQLOrderBy()
                : this(OrderType.ASCENDING)
            {
            }

            public SQLOrderBy(OrderType type)
                : base("ORDER BY ")
            {
                Type = type;
            }

            internal override string GetQueryString()
            {
                Query = new StringBuilder(base.GetQueryString());

                if (Type == OrderType.DESCENDING)
                    Query.Append(" DESCENDING");

                return Query.ToString();
            }
        } // End class SQLOrderBy

        #endregion SQLite Helper Classes

    } // End class SQLiteWrapper
}
