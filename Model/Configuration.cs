﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using InventorySetup.Controller;
using System.IO;
using System.Xml;
using NLog;
using System.Xml.Linq;

namespace InventorySetup.Model
{
    public static class Configuration
    {
        private static string SettingsFile = Path.Combine(Utils.BasePath, "Configuration.xml");
        private static Logger log = LogManager.GetLogger("Configuration");
        private static XDocument Xml;

        public static void ReadSettings()
        {
            if (Xml == null)
            {
                Xml = XDocument.Load(SettingsFile);
            }
        }

        public static string Get(string key)
        {
            log.Debug("Request for {0}", key);
            ReadSettings();

            foreach(XElement e in Xml.Descendants("settings").Nodes())
            {
                if (e.Name == key)
                    return e.Value;
            }

            return "";
        }

        /*
        public static XmlNode GetNode(string key)
        {
            log.Debug("Request for {0} node", key);
            ReadSettings();
            foreach (XmlNode node in Xml.ChildNodes[0])
            {
                if (node.Name == key)
                    return node;
            }

            return null;
        }

        public static void Set(string key, string value)
        {
            ReadSettings();
            XmlNode node = GetNode(key);

            if (node == null)
            {
                XmlElement e = Xml.CreateElement(key);
                e.Value = value;
                Xml.ChildNodes[0].AppendChild(e);
            }
            else
            {
                node.Value = value;
            }

            Xml.Save(SettingsFile);
        }

        public static string GetAttributeValue(XmlAttributeCollection atts, string key)
        {
            ReadSettings();
            if (atts.Count > 0)
            {
                foreach (XmlAttribute a in atts)
                {
                    if (a.Name == key)
                        return a.Value;
                }
            }

            return "";
        }*/
    }
}
