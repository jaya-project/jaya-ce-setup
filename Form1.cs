﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using InventorySetup.Model;
using InventorySetup.Controller;
using NLog;
using System.IO;

namespace InventorySetup
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Setup()
        {
            try
            {
                Logger l = LogManager.GetLogger("Form1");
                l.Debug("Path \\DiskOnChip exists {0}?", Directory.Exists("\\DiskOnChip"));
                string dbFile = Configuration.Get("dbFile");

                if(File.Exists(dbFile))
                {
                    SQLiteWrapper wrap = new SQLiteWrapper(dbFile);
                    SessionHelper sh = new SessionHelper();
                    string tabl = Utils.GetTableName("user");
                    byte[] salt = sh.MakeSalt(50);
                    byte[] hash = sh.MakeHash(tbPass.Text, salt);
                    
                    string cols = "name, last_name, rut, nickname, password, salt";
                    List<List<object>> data = new List<List<object>>()
                    {
                        new List<object>
                        {
                            "Techwert", "Admin", "1-9", "admin", hash, salt
                        }
                    };

                    int update = wrap.Insert(tabl, cols, data, "");

                    if (update > 0)
                        MessageBox.Show("User Created!");
                    else
                        MessageBox.Show("Could not create the user!");
                }
                else
                {
                    l.Debug("The file {0} does not exists", dbFile);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            Setup();
        }
    }
}