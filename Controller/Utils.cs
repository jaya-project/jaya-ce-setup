﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using InventorySetup.Model;

namespace InventorySetup.Controller
{
    class Utils
    {
        private static string BPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static string BasePath
        {
            get
            {
                return BPath;
            }
        }

        public static bool IsNumber(string data)
        {
            return Regex.IsMatch(data, "^[0-9]+$", RegexOptions.Compiled);
        }

        public static string GetTableName(string table)
        {
            string s = "";

            if (table.Length > 0)
            {
                s = string.Format("{0}_{1}", Configuration.Get("tablePrefix"), table);
            }

            return s;
        }
    }
}
