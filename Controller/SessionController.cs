﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Globalization;
using InventoryCE.Model;
using NLog;

namespace InventorySetup.Controller
{
    public static class SessionController
    {
        public enum SessionEvent
        {
            ERROR = 0, LOGIN, LOGOUT
        }

        private static string User;
        private static long ID = 0;
        private static long SessionID = 0;
        private static byte LoggedIn = 0;
        private static Logger log = LogManager.GetLogger("LoginController");
        private static SQLiteWrapper Sqlite = new SQLiteWrapper(Configuration.Get("dbFile"));
        private static bool Expire = true;
        private static int Duration = 15;
        private static DateTime Started;
        
        public static bool SessionExpire {
            get
            {
                return Expire;
            }

            set
            {
                Expire = value;
            }
        }

        public static int SessionDuration
        {
            get
            {
                return Duration;
            }

            set
            {
                Duration = value;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return LoggedIn == 1;
            }
        }

        public static DateTime LastActive
        {
            get;
            set;
        }

        public static bool Authenticate(string userName, string password)
        {
            try
            {
                User = userName;
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("nickname = {0}", userName);
                string table = Utils.GetTableName("user");
                DataTable dt = Sqlite.Query(table, "password, salt, id", where);
                where = null;

                if (dt.Rows.Count == 1)
                {
                    SessionHelper sh = new SessionHelper();
                    ID = long.Parse(dt.Rows[0]["id"].ToString());
                    byte[] pass = (byte[])(dt.Rows[0]["password"].ToString());
                    byte[] salt = (byte[])(dt.Rows[0]["salt"].ToString());
                    byte[] hash = sh.MakeHash(password, salt);

                    if (sh.CompareHash(hash, pass))
                    {
                        return StartSession();
                    }
                    else
                    {
                        log.Warn("Invalid login attempt. User: [{0}]", userName);
                    }

                    sh = null;
                }

                dt = null;
                table = "";
                
            }
            catch (Exception e)
            {
                log.Error("Authentication failed: {0}", e.StackTrace);
            }

            return false;
        }

        private static bool StartSession()
        {
            try
            {
                string table = Utils.GetTableName("session");
                string cols = "event_time, event_name, user_id";
                DateTime now = DateTime.Now;
                Started = now;

                List<List<object>> data = new List<List<object>>()
                {
                    new List<object>()
                    {
                        Started.ToString(CultureInfo.CurrentCulture),
                        SessionEvent.LOGIN.ToString(),
                        ID.ToString()
                    }
                };

                int updated = Sqlite.Insert(table, cols, data, "");

                if (updated > 0 && SetSessionID(SessionEvent.LOGIN))
                {
                    LoggedIn = 1;
                }
                else
                {
                    log.Error("Could not register session data!");
                }
            }
            catch (Exception e)
            {
                log.Error("Session start error: {0}", e.StackTrace);
            }

            return false;
        }

        public static void UpdateSession()
        {
            if (Expire)
            {
                try
                {
                    string table = Utils.GetTableName("session");
                    DateTime newDate = Started;
                    newDate.AddMinutes((double)Duration);

                    if (newDate.CompareTo(Started) > 0)
                    {
                        Logout(newDate);
                    }

                    table = "";
                }
                catch (Exception e)
                {
                    log.Error("Error trying to update session data: {0}", e.StackTrace);
                }
            }
        }

        public static bool Logout(DateTime date)
        {
            if (date == null)
                date = DateTime.Now;

            string table = Utils.GetTableName("session");

            try
            {
                int updated = Sqlite.Insert(table, "event_time, event_name, user_id", new List<List<object>>
                {
                    new List<object>()
                    {
                        date.ToString(CultureInfo.CurrentCulture),
                        SessionEvent.LOGOUT.ToString(),
                        ID.ToString()
                    }
                }, "");

                if (updated > 0)
                {
                    log.Info("Successfully logged out: {0}", User);
                    LoggedIn = 0;
                    ID = 0;
                    SessionID = 0;
                    User = "";
                    return true;
                }
                else
                {
                    log.Error("Could not Logout!");
                }
            }
            catch (Exception e)
            {
                log.Error("An error ocurred trying to logout: {0}", e.StackTrace);
            }

            return true;
        }

        private static bool SetSessionID(SessionEvent se)
        {
            try
            {
                string table = Utils.GetTableName("session");
                SQLiteWrapper.SQLWhere where = new SQLiteWrapper.SQLWhere();
                where.Append("event_time = {0} AND event_name = {1} AND user_id = {2}", new object[] {
                    Started, se, ID
                });
                DataTable t = Sqlite.Query(table, "id", where);

                if (t != null)
                {
                    SessionID = long.Parse(t.Rows[0]["id"].ToString());
                    return true;
                }
                else
                {
                    log.Error("SessionController.SetSessionID: Could not set Session ID!");
                }

                t.Dispose();
            }
            catch (Exception e)
            {
                log.Error("Error trying to stablish session ID: {0}", e.StackTrace);
            }

            return false;
        }
    } // End class LoginController
}
